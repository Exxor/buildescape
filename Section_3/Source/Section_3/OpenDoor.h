// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Engine/TriggerVolume.h"
#include "OpenDoor.generated.h"


UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class SECTION_3_API UOpenDoor : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOpenDoor();

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	void closeDoor();
	void openDoor();

private:
	UPROPERTY(EditAnywhere)
	float openDeg = -90.0f;

	UPROPERTY(EditAnywhere)
	float doorCloseDelay = 1.f;

	UPROPERTY(EditAnywhere)
	ATriggerVolume* pressurePlate;

	float lastDoorOpenTime;


	AActor* actorThatOpens; 

	AActor *owner; // gets the door
};
